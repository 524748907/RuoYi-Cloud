package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.AppCostCategoryRatesMapper;
import com.ruoyi.app.domain.AppCostCategoryRates;
import com.ruoyi.app.service.IAppCostCategoryRatesService;

/**
 * 收费标准Service业务层处理
 * 
 * @author ziye
 * @date 2020-07-08
 */
@Service
public class AppCostCategoryRatesServiceImpl implements IAppCostCategoryRatesService 
{
    @Autowired
    private AppCostCategoryRatesMapper appCostCategoryRatesMapper;



    /**
     * 查询收费标准
     * 
     * @param id 收费标准ID
     * @return 收费标准
     */
    @Override
    public AppCostCategoryRates selectAppCostCategoryRatesById(Long id)
    {
        return appCostCategoryRatesMapper.selectAppCostCategoryRatesById(id);
    }

    /**
     * 查询收费标准列表
     * 
     * @param appCostCategoryRates 收费标准
     * @return 收费标准
     */
    @Override
    public List<AppCostCategoryRates> selectAppCostCategoryRatesList(AppCostCategoryRates appCostCategoryRates)
    {
        return appCostCategoryRatesMapper.selectAppCostCategoryRatesList(appCostCategoryRates);
    }

    /**
     * 新增收费标准
     * 
     * @param appCostCategoryRates 收费标准
     * @return 结果
     */
    @Override
    public int insertAppCostCategoryRates(AppCostCategoryRates appCostCategoryRates)
    {
        appCostCategoryRates.setCreateTime(DateUtils.getNowDate());
        return appCostCategoryRatesMapper.insertAppCostCategoryRates(appCostCategoryRates);
    }

    /**
     * 修改收费标准
     * 
     * @param appCostCategoryRates 收费标准
     * @return 结果
     */
    @Override
    public int updateAppCostCategoryRates(AppCostCategoryRates appCostCategoryRates)
    {
        return appCostCategoryRatesMapper.updateAppCostCategoryRates(appCostCategoryRates);
    }

    /**
     * 批量删除收费标准
     * 
     * @param ids 需要删除的收费标准ID
     * @return 结果
     */
    @Override
    public int deleteAppCostCategoryRatesByIds(Long[] ids)
    {
        return appCostCategoryRatesMapper.deleteAppCostCategoryRatesByIds(ids);
    }

    /**
     * 删除收费标准信息
     * 
     * @param id 收费标准ID
     * @return 结果
     */
    @Override
    public int deleteAppCostCategoryRatesById(Long id)
    {
        return appCostCategoryRatesMapper.deleteAppCostCategoryRatesById(id);
    }

    @Override
    public int deleteAppCostCategoryRatesByCid(Long cid) {
        return appCostCategoryRatesMapper.deleteAppCostCategoryRatesByCid(cid);
    }
}
