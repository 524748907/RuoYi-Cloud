package com.ruoyi.app.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.domain.AppCostProject;

/**
 * 收费项目Service接口
 * 
 * @author ziye
 * @date 2020-07-08
 */
public interface IAppCostProjectService 
{
    /**
     * 查询收费项目
     * 
     * @param id 收费项目ID
     * @return 收费项目
     */
    public AppCostProject selectAppCostProjectById(Long id);

    /**
     * 查询收费项目列表
     * 
     * @param appCostProject 收费项目
     * @return 收费项目集合
     */
    public List<AppCostProject> selectAppCostProjectList(AppCostProject appCostProject);

    /**
     * 新增收费项目
     * 
     * @param appCostProject 收费项目
     * @return 结果
     */
    public int insertAppCostProject(AppCostProject appCostProject);

    /**
     * 修改收费项目
     * 
     * @param appCostProject 收费项目
     * @return 结果
     */
    public int updateAppCostProject(AppCostProject appCostProject);

    /**
     * 批量删除收费项目
     * 
     * @param ids 需要删除的收费项目ID
     * @return 结果
     */
    public int deleteAppCostProjectByIds(Long[] ids);

    /**
     * 删除收费项目信息
     * 
     * @param id 收费项目ID
     * @return 结果
     */
    public int deleteAppCostProjectById(Long id);

    public List<AppCostProject> queryCompanyTypeList();
}
