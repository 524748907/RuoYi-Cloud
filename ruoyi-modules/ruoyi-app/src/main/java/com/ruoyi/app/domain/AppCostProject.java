package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 收费项目对象 app_cost_project
 * 
 * @author ziye
 * @date 2020-07-08
 */
public class AppCostProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 收费项目名称 */
    @Excel(name = "收费项目名称")
    private String name;

    /** 公司类型 1:单位 2:个人 3:经营者 */
    @Excel(name = "公司类型 1:单位 2:个人 3:经营者")
    private String companyType;

    /** 颜色 */
    @Excel(name = "颜色")
    private String color;

    /** 排序（值越大越靠前） */
    @Excel(name = "排序", readConverterExp = "值=越大越靠前")
    private Long sort;

    /** 是否显示 */
    @Excel(name = "是否显示")
    private String hide;

    /** 删除标记 */
    private String delFlag;

    private List<AppCostCategoryRates> children;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCompanyType(String companyType) 
    {
        this.companyType = companyType;
    }

    public String getCompanyType() 
    {
        return companyType;
    }
    public void setColor(String color) 
    {
        this.color = color;
    }

    public String getColor() 
    {
        return color;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setHide(String hide) 
    {
        this.hide = hide;
    }

    public String getHide() 
    {
        return hide;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public List<AppCostCategoryRates> getChildren() {
        return children;
    }

    public void setChildren(List<AppCostCategoryRates> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("companyType", getCompanyType())
            .append("color", getColor())
            .append("sort", getSort())
            .append("hide", getHide())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
