package com.ruoyi.app.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.domain.AppCostCompany;
import com.ruoyi.app.util.circle.Point2D;
import org.apache.ibatis.annotations.Param;

/**
 * 店铺管理Service接口
 * 
 * @author ziye
 * @date 2020-07-11
 */
public interface IAppCostCompanyService 
{
    /**
     * 查询店铺管理
     * 
     * @param id 店铺管理ID
     * @return 店铺管理
     */
    public AppCostCompany selectAppCostCompanyById(Long id);

    /**
     * 查询店铺管理列表
     * 
     * @param appCostCompany 店铺管理
     * @return 店铺管理集合
     */
    public List<AppCostCompany> selectAppCostCompanyList(AppCostCompany appCostCompany);

    /**
     * 新增店铺管理
     * 
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    public int insertAppCostCompany(AppCostCompany appCostCompany);

    /**
     * 修改店铺管理
     * 
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    public int updateAppCostCompany(AppCostCompany appCostCompany);

    /**
     * 批量删除店铺管理
     * 
     * @param ids 需要删除的店铺管理ID
     * @return 结果
     */
    public int deleteAppCostCompanyByIds(Long[] ids);

    /**
     * 删除店铺管理信息
     * 
     * @param id 店铺管理ID
     * @return 结果
     */
    public int deleteAppCostCompanyById(Long id);

    /**
     * 查询所有经纬度
     * @return
     */
    public List<Point2D> queryPointList(AppCostCompany appCostCompany);

    /**
     * 加载所有经纬度
     * @return
     */
    public List<Map> queryLatLngList(Double lat, Double lng, Integer distance);

    /**
     * 获取登录用户信息
     * @param userId
     * @return
     */
    public Map getUserInfo(Long userId);

    /**
     * 获取完善信息用户列表
     * @return
     */
    public List<Map> queryUserList();

    /**
     * 更新用户围栏(废弃)
     * @param id
     * @param enclosure
     * @param color
     * @return
     */
    public int updateUser(Long id, String enclosure, String color);

    /**
     * 获取配置信息
     * @param key
     * @return
     */
    public String getConfigValue(String key);

    public int updatePointList(AppCostCompany appCostCompany);

    public List<Map> querySearchList(Long userId, String title);
}
