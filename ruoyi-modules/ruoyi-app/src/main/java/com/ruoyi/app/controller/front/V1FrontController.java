package com.ruoyi.app.controller.front;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ruoyi.app.config.AppConfig;
import com.ruoyi.app.domain.AppCostCategoryRates;
import com.ruoyi.app.domain.AppCostCompany;
import com.ruoyi.app.domain.AppCostProject;
import com.ruoyi.app.service.IAppCostCategoryRatesService;
import com.ruoyi.app.service.IAppCostCompanyService;
import com.ruoyi.app.service.IAppCostProjectService;
import com.ruoyi.app.util.CompanyUtils;
import com.ruoyi.common.core.utils.FileUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/v1")
public class V1FrontController {

    @Autowired
    private IAppCostCompanyService appCostCompanyService;

    /**
     * 在线生气
     * @return
     */
    @GetMapping("/checkVersion")
    public AjaxResult checkVersion(){
        Map result = Maps.newConcurrentMap();
        result.put("version", appCostCompanyService.getConfigValue("app.version"));
        result.put("download", appCostCompanyService.getConfigValue("app.download"));
        return AjaxResult.success("OK", result);
    }
}
