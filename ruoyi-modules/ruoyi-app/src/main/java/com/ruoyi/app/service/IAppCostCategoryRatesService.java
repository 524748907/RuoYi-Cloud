package com.ruoyi.app.service;

import java.util.List;
import com.ruoyi.app.domain.AppCostCategoryRates;

/**
 * 收费标准Service接口
 * 
 * @author ziye
 * @date 2020-07-08
 */
public interface IAppCostCategoryRatesService 
{
    /**
     * 查询收费标准
     * 
     * @param id 收费标准ID
     * @return 收费标准
     */
    public AppCostCategoryRates selectAppCostCategoryRatesById(Long id);

    /**
     * 查询收费标准列表
     * 
     * @param appCostCategoryRates 收费标准
     * @return 收费标准集合
     */
    public List<AppCostCategoryRates> selectAppCostCategoryRatesList(AppCostCategoryRates appCostCategoryRates);

    /**
     * 新增收费标准
     * 
     * @param appCostCategoryRates 收费标准
     * @return 结果
     */
    public int insertAppCostCategoryRates(AppCostCategoryRates appCostCategoryRates);

    /**
     * 修改收费标准
     * 
     * @param appCostCategoryRates 收费标准
     * @return 结果
     */
    public int updateAppCostCategoryRates(AppCostCategoryRates appCostCategoryRates);

    /**
     * 批量删除收费标准
     * 
     * @param ids 需要删除的收费标准ID
     * @return 结果
     */
    public int deleteAppCostCategoryRatesByIds(Long[] ids);

    /**
     * 删除收费标准信息
     * 
     * @param id 收费标准ID
     * @return 结果
     */
    public int deleteAppCostCategoryRatesById(Long id);

    public int deleteAppCostCategoryRatesByCid(Long cid);
}
