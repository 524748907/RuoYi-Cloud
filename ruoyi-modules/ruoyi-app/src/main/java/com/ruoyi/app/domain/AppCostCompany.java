package com.ruoyi.app.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 店铺管理对象 app_cost_company
 * 
 * @author ziye
 * @date 2020-07-11
 */
public class AppCostCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 收费项目编号 */
    @Excel(name = "收费项目编号")
    private Long cid;

    /** 收费项目名称 */
    @Excel(name = "收费项目名称")
    private String cname;

    /** 收费单价 */
    @Excel(name = "收费单价")
    private BigDecimal cmoney;

    /** 收费单位 */
    @Excel(name = "收费单位")
    private String cunit;

    /** 最小单位 */
    @Excel(name = "最小单位")
    private BigDecimal cmin;

    /** 最大单位 */
    @Excel(name = "最大单位")
    private BigDecimal cmax;

    /** 收费项目ID */
    @Excel(name = "收费项目ID")
    private Long projectId;

    /** 收费项目 */
    @Excel(name = "收费项目")
    private String projectName;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String title;

    /** 门头照 */
    @Excel(name = "门头照")
    private String companyPic;

    /** 纬度 */
    @Excel(name = "纬度")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 营业执照 */
    @Excel(name = "营业执照")
    private String licensePic;

    private String companyName;

    /** 公司法人 */
    @Excel(name = "公司法人")
    private String name;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String mobile;

    /** 公司地址 */
    @Excel(name = "公司地址")
    private String address;

    /** 经营范围 */
    @Excel(name = "经营范围")
    private String businessScope;

    /** 执照编号 */
    @Excel(name = "执照编号")
    private String businessCode;

    /** 缴费类型 0:未缴费  1:已缴费 */
    @Excel(name = "缴费类型 0:未缴费  1:已缴费")
    private String costType;

    /** 公司类型 */
    @Excel(name = "公司类型")
    private String costCompnayType;

    /** 收费标准 */
    @Excel(name = "收费标准")
    private Double costStandard;

    /** 应收金额 */
    @Excel(name = "应收金额")
    private Double totalMoney;

    /** 目前已缴费金额 */
    @Excel(name = "目前已缴费金额")
    private Double money;

    /** 清运单位 */
    @Excel(name = "清运单位")
    private String clearUnit;

    /** 垃圾生产量 */
    @Excel(name = "垃圾生产量")
    private String clearNum;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String clearName;

    /** 联系人姓名 */
    @Excel(name = "联系人姓名")
    private String clearMobile;

    /** 清运公司名称 */
    @Excel(name = "清运公司名称")
    private String clearCompanyName;

    /** 是否隐藏  */
    @Excel(name = "是否隐藏 ")
    private Integer hide;

    /** 删除标记 */
    private Integer delFlag;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 归属部门编号 */
    @Excel(name = "归属部门编号")
    private Long deptId;

    /** 归属部门 */
    @Excel(name = "归属部门")
    private String deptName;

    private Long createId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCid(Long cid) 
    {
        this.cid = cid;
    }

    public Long getCid() 
    {
        return cid;
    }
    public void setCname(String cname) 
    {
        this.cname = cname;
    }

    public String getCname() 
    {
        return cname;
    }
    public void setCmoney(BigDecimal cmoney) 
    {
        this.cmoney = cmoney;
    }

    public BigDecimal getCmoney() 
    {
        return cmoney;
    }
    public void setCunit(String cunit) 
    {
        this.cunit = cunit;
    }

    public String getCunit() 
    {
        return cunit;
    }
    public void setCmin(BigDecimal cmin) 
    {
        this.cmin = cmin;
    }

    public BigDecimal getCmin() 
    {
        return cmin;
    }
    public void setCmax(BigDecimal cmax) 
    {
        this.cmax = cmax;
    }

    public BigDecimal getCmax() 
    {
        return cmax;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setCompanyPic(String companyPic) 
    {
        this.companyPic = companyPic;
    }

    public String getCompanyPic() 
    {
        return companyPic;
    }
    public void setLat(Double lat)
    {
        this.lat = lat;
    }

    public Double getLat()
    {
        return lat;
    }
    public void setLng(Double lng)
    {
        this.lng = lng;
    }

    public Double getLng()
    {
        return lng;
    }
    public void setLicensePic(String licensePic) 
    {
        this.licensePic = licensePic;
    }

    public String getLicensePic() 
    {
        return licensePic;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setBusinessScope(String businessScope) 
    {
        this.businessScope = businessScope;
    }

    public String getBusinessScope() 
    {
        return businessScope;
    }
    public void setBusinessCode(String businessCode) 
    {
        this.businessCode = businessCode;
    }

    public String getBusinessCode() 
    {
        return businessCode;
    }
    public void setCostType(String costType) 
    {
        this.costType = costType;
    }

    public String getCostType() 
    {
        return costType;
    }
    public void setCostCompnayType(String costCompnayType) 
    {
        this.costCompnayType = costCompnayType;
    }

    public String getCostCompnayType()
    {
        return costCompnayType;
    }
    public void setCostStandard(Double costStandard)
    {
        this.costStandard = costStandard;
    }

    public Double getCostStandard()
    {
        return costStandard;
    }
    public void setTotalMoney(Double totalMoney)
    {
        this.totalMoney = totalMoney;
    }

    public Double getTotalMoney()
    {
        return totalMoney;
    }
    public void setMoney(Double money)
    {
        this.money = money;
    }

    public Double getMoney()
    {
        return money;
    }
    public void setClearUnit(String clearUnit)
    {
        this.clearUnit = clearUnit;
    }

    public String getClearUnit()
    {
        return clearUnit;
    }
    public void setClearNum(String clearNum)
    {
        this.clearNum = clearNum;
    }

    public String getClearNum()
    {
        return clearNum;
    }
    public void setClearName(String clearName)
    {
        this.clearName = clearName;
    }

    public String getClearName()
    {
        return clearName;
    }
    public void setClearMobile(String clearMobile) 
    {
        this.clearMobile = clearMobile;
    }

    public String getClearMobile() 
    {
        return clearMobile;
    }
    public void setClearCompanyName(String clearCompanyName)
    {
        this.clearCompanyName = clearCompanyName;
    }

    public String getClearCompanyName()
    {
        return clearCompanyName;
    }
    public void setHide(Integer hide) 
    {
        this.hide = hide;
    }

    public Integer getHide() 
    {
        return hide;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setDeptName(String deptName) 
    {
        this.deptName = deptName;
    }

    public String getDeptName() 
    {
        return deptName;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cid", getCid())
            .append("cname", getCname())
            .append("cmoney", getCmoney())
            .append("cunit", getCunit())
            .append("cmin", getCmin())
            .append("cmax", getCmax())
            .append("projectId", getProjectId())
            .append("projectName", getProjectName())
            .append("title", getTitle())
            .append("companyPic", getCompanyPic())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("licensePic", getLicensePic())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("businessScope", getBusinessScope())
            .append("businessCode", getBusinessCode())
            .append("costType", getCostType())
            .append("costCompnayType", getCostCompnayType())
            .append("costStandard", getCostStandard())
            .append("totalMoney", getTotalMoney())
            .append("money", getMoney())
            .append("clearUnit", getClearUnit())
            .append("clearNum", getClearNum())
            .append("clearName", getClearName())
            .append("clearMobile", getClearMobile())
            .append("clearCompanyName", getClearCompanyName())
            .append("hide", getHide())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .append("state", getState())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
