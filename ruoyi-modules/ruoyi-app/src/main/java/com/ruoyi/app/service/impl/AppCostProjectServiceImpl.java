package com.ruoyi.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.app.domain.AppCostCategoryRates;
import com.ruoyi.app.service.IAppCostCategoryRatesService;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.AppCostProjectMapper;
import com.ruoyi.app.domain.AppCostProject;
import com.ruoyi.app.service.IAppCostProjectService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 收费项目Service业务层处理
 * 
 * @author ziye
 * @date 2020-07-08
 */
@Service
public class AppCostProjectServiceImpl implements IAppCostProjectService 
{
    @Autowired
    private AppCostProjectMapper appCostProjectMapper;

    @Autowired
    private IAppCostCategoryRatesService appCostCategoryRatesService;

    /**
     * 查询收费项目
     * 
     * @param id 收费项目ID
     * @return 收费项目
     */
    @Override
    public AppCostProject selectAppCostProjectById(Long id)
    {
        AppCostProject appCostProject = appCostProjectMapper.selectAppCostProjectById(id);
        AppCostCategoryRates param = new AppCostCategoryRates();
        param.setCid(id);
        List<AppCostCategoryRates> children = appCostCategoryRatesService.selectAppCostCategoryRatesList(param);
        if(children != null && children.size() > 0){
            appCostProject.setChildren(children);
        }
        return appCostProject;
    }

    /**
     * 查询收费项目列表
     * 
     * @param appCostProject 收费项目
     * @return 收费项目
     */
    @Override
    public List<AppCostProject> selectAppCostProjectList(AppCostProject appCostProject)
    {
        return appCostProjectMapper.selectAppCostProjectList(appCostProject);
    }

    /**
     * 新增收费项目
     * 
     * @param appCostProject 收费项目
     * @return 结果
     */
    @Override
    @Transactional
    public int insertAppCostProject(AppCostProject appCostProject)
    {
        appCostProject.setDelFlag("0");
        appCostProject.setCreateTime(DateUtils.getNowDate());
        int rows = appCostProjectMapper.insertAppCostProject(appCostProject);
        appCostCategoryRatesService.deleteAppCostCategoryRatesByCid(appCostProject.getId());
        List<AppCostCategoryRates> children = appCostProject.getChildren();
        for (AppCostCategoryRates costCategoryRates : children) {
            costCategoryRates.setCid(appCostProject.getId());
            costCategoryRates.setCname(appCostProject.getName());
            appCostCategoryRatesService.insertAppCostCategoryRates(costCategoryRates);
        }
        return rows;
    }

    /**
     * 修改收费项目
     * 
     * @param appCostProject 收费项目
     * @return 结果
     */
    @Override
    @Transactional
    public int updateAppCostProject(AppCostProject appCostProject)
    {
        int rows = appCostProjectMapper.updateAppCostProject(appCostProject);
        appCostCategoryRatesService.deleteAppCostCategoryRatesByCid(appCostProject.getId());
        List<AppCostCategoryRates> children = appCostProject.getChildren();
        for (AppCostCategoryRates costCategoryRates : children) {
            costCategoryRates.setCid(appCostProject.getId());
            costCategoryRates.setCname(appCostProject.getName());
            appCostCategoryRatesService.insertAppCostCategoryRates(costCategoryRates);
        }
        return rows;
    }

    /**
     * 批量删除收费项目
     * 
     * @param ids 需要删除的收费项目ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteAppCostProjectByIds(Long[] ids)
    {
        int rows = appCostProjectMapper.deleteAppCostProjectByIds(ids);
        for (Long cid : ids) {
            appCostCategoryRatesService.deleteAppCostCategoryRatesByCid(cid);
        }
        return rows;
    }

    /**
     * 删除收费项目信息
     * 
     * @param id 收费项目ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteAppCostProjectById(Long id)
    {
        int rows = appCostProjectMapper.deleteAppCostProjectById(id);
        appCostCategoryRatesService.deleteAppCostCategoryRatesByCid(id);
        return rows;
    }

    @Override
    public List<AppCostProject> queryCompanyTypeList() {
        return appCostProjectMapper.queryCompanyTypeList();
    }
}
