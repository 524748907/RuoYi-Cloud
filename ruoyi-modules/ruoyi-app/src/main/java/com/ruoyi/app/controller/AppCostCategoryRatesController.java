package com.ruoyi.app.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.app.domain.AppCostCategoryRates;
import com.ruoyi.app.service.IAppCostCategoryRatesService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 收费标准Controller
 * 
 * @author ziye
 * @date 2020-07-08
 */
@RestController
@RequestMapping("/appcostcategoryrates")
public class AppCostCategoryRatesController extends BaseController
{
    @Autowired
    private IAppCostCategoryRatesService appCostCategoryRatesService;

    /**
     * 查询收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppCostCategoryRates appCostCategoryRates)
    {
        startPage();
        List<AppCostCategoryRates> list = appCostCategoryRatesService.selectAppCostCategoryRatesList(appCostCategoryRates);
        return getDataTable(list);
    }

    /**
     * 导出收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:export')")
    @Log(title = "收费标准", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppCostCategoryRates appCostCategoryRates) throws IOException
    {
        List<AppCostCategoryRates> list = appCostCategoryRatesService.selectAppCostCategoryRatesList(appCostCategoryRates);
        ExcelUtil<AppCostCategoryRates> util = new ExcelUtil<AppCostCategoryRates>(AppCostCategoryRates.class);
        util.exportExcel(response, list, "appcostcategoryrates");
    }

    /**
     * 获取收费标准详细信息
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(appCostCategoryRatesService.selectAppCostCategoryRatesById(id));
    }

    /**
     * 新增收费标准
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:add')")
    @Log(title = "收费标准", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppCostCategoryRates appCostCategoryRates)
    {
        return toAjax(appCostCategoryRatesService.insertAppCostCategoryRates(appCostCategoryRates));
    }

    /**
     * 修改收费标准
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:edit')")
    @Log(title = "收费标准", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppCostCategoryRates appCostCategoryRates)
    {
        return toAjax(appCostCategoryRatesService.updateAppCostCategoryRates(appCostCategoryRates));
    }

    /**
     * 删除收费标准
     */
    @PreAuthorize("@ss.hasPermi('app:appcostcategoryrates:remove')")
    @Log(title = "收费标准", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appCostCategoryRatesService.deleteAppCostCategoryRatesByIds(ids));
    }
}
