package com.ruoyi.app.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.util.circle.Point2D;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.AppCostCompanyMapper;
import com.ruoyi.app.domain.AppCostCompany;
import com.ruoyi.app.service.IAppCostCompanyService;

/**
 * 店铺管理Service业务层处理
 * 
 * @author ziye
 * @date 2020-07-11
 */
@Service
public class AppCostCompanyServiceImpl implements IAppCostCompanyService 
{
    @Autowired
    private AppCostCompanyMapper appCostCompanyMapper;

    /**
     * 查询店铺管理
     * 
     * @param id 店铺管理ID
     * @return 店铺管理
     */
    @Override
    public AppCostCompany selectAppCostCompanyById(Long id)
    {
        return appCostCompanyMapper.selectAppCostCompanyById(id);
    }

    /**
     * 查询店铺管理列表
     * 
     * @param appCostCompany 店铺管理
     * @return 店铺管理
     */
    @Override
    public List<AppCostCompany> selectAppCostCompanyList(AppCostCompany appCostCompany)
    {
        return appCostCompanyMapper.selectAppCostCompanyList(appCostCompany);
    }

    /**
     * 新增店铺管理
     * 
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    @Override
    public int insertAppCostCompany(AppCostCompany appCostCompany)
    {
        appCostCompany.setCreateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(appCostCompany.getCompanyPic()) && StringUtils.isNotEmpty(appCostCompany.getMobile()) && StringUtils.isNotEmpty(appCostCompany.getLicensePic()) && appCostCompany.getCostStandard() != null && appCostCompany.getCostStandard() > 0){
            appCostCompany.setState("3");
        }else if(StringUtils.isNotEmpty(appCostCompany.getCompanyPic()) || StringUtils.isNotEmpty(appCostCompany.getMobile()) || StringUtils.isNotEmpty(appCostCompany.getLicensePic()) || (appCostCompany.getCostStandard() != null && appCostCompany.getCostStandard() > 0)){
            appCostCompany.setState("2");
        }else{
            appCostCompany.setState("1");
        }
        return appCostCompanyMapper.insertAppCostCompany(appCostCompany);
    }

    /**
     * 修改店铺管理
     * 
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    @Override
    public int updateAppCostCompany(AppCostCompany appCostCompany)
    {
        if(StringUtils.isNotEmpty(appCostCompany.getCompanyPic()) && StringUtils.isNotEmpty(appCostCompany.getMobile()) && StringUtils.isNotEmpty(appCostCompany.getLicensePic()) && appCostCompany.getCostStandard() != null && appCostCompany.getCostStandard() > 0){
            appCostCompany.setState("3");
        }else if(StringUtils.isNotEmpty(appCostCompany.getMobile()) || StringUtils.isNotEmpty(appCostCompany.getLicensePic()) || (appCostCompany.getCostStandard() != null && appCostCompany.getCostStandard() > 0)){
            appCostCompany.setState("2");
        }else{
            appCostCompany.setState("1");
        }
        return appCostCompanyMapper.updateAppCostCompany(appCostCompany);
    }

    /**
     * 批量删除店铺管理
     * 
     * @param ids 需要删除的店铺管理ID
     * @return 结果
     */
    @Override
    public int deleteAppCostCompanyByIds(Long[] ids)
    {
        return appCostCompanyMapper.deleteAppCostCompanyByIds(ids);
    }

    /**
     * 删除店铺管理信息
     * 
     * @param id 店铺管理ID
     * @return 结果
     */
    @Override
    public int deleteAppCostCompanyById(Long id)
    {
        return appCostCompanyMapper.deleteAppCostCompanyById(id);
    }

    @Override
    public List<Point2D> queryPointList(AppCostCompany appCostCompany) {
        return appCostCompanyMapper.queryPointList(appCostCompany);
    }

    @Override
    public List<Map> queryLatLngList(Double lat, Double lng, Integer distance) {
        return appCostCompanyMapper.queryLatLngList(lat, lng, distance);
    }

    @Override
    public Map getUserInfo(Long userId) {
        return appCostCompanyMapper.getUserInfo(userId);
    }

    @Override
    public List<Map> queryUserList() {
        return appCostCompanyMapper.queryUserList();
    }

    @Override
    public int updateUser(Long id, String enclosure, String color) {
        return appCostCompanyMapper.updateUser(id, enclosure, color);
    }

    @Override
    public String getConfigValue(String key) {
        return appCostCompanyMapper.getConfigValue(key);
    }

    @Override
    public int updatePointList(AppCostCompany appCostCompany) {
        return appCostCompanyMapper.updatePointList(appCostCompany);
    }

    @Override
    public List<Map> querySearchList(Long userId, String title) {
        return appCostCompanyMapper.querySearchList(userId, title);
    }
}
