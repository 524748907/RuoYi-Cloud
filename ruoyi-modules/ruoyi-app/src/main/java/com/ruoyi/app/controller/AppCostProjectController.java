package com.ruoyi.app.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.app.service.IAppCostCategoryRatesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.app.domain.AppCostProject;
import com.ruoyi.app.service.IAppCostProjectService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 收费项目Controller
 * 
 * @author ziye
 * @date 2020-07-08
 */
@RestController
@RequestMapping("/appcostproject")
public class AppCostProjectController extends BaseController
{
    @Autowired
    private IAppCostProjectService appCostProjectService;

    /**
     * 查询收费项目列表
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppCostProject appCostProject)
    {
        startPage();
        List<AppCostProject> list = appCostProjectService.selectAppCostProjectList(appCostProject);
        return getDataTable(list);
    }

    /**
     * 导出收费项目列表
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:export')")
    @Log(title = "收费项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppCostProject appCostProject) throws IOException
    {
        List<AppCostProject> list = appCostProjectService.selectAppCostProjectList(appCostProject);
        ExcelUtil<AppCostProject> util = new ExcelUtil<AppCostProject>(AppCostProject.class);
        util.exportExcel(response, list, "appcostproject");
    }

    /**
     * 获取收费项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(appCostProjectService.selectAppCostProjectById(id));
    }

    /**
     * 新增收费项目
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:add')")
    @Log(title = "收费项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppCostProject appCostProject)
    {
        return toAjax(appCostProjectService.insertAppCostProject(appCostProject));
    }

    /**
     * 修改收费项目
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:edit')")
    @Log(title = "收费项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppCostProject appCostProject)
    {
        return toAjax(appCostProjectService.updateAppCostProject(appCostProject));
    }

    /**
     * 删除收费项目
     */
    @PreAuthorize("@ss.hasPermi('app:appcostproject:remove')")
    @Log(title = "收费项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appCostProjectService.deleteAppCostProjectByIds(ids));
    }
}
