package com.ruoyi.app.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 收费标准对象 app_cost_category_rates
 * 
 * @author ziye
 * @date 2020-07-08
 */
public class AppCostCategoryRates extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 收费项目编号 */
    @Excel(name = "收费项目编号")
    private Long cid;

    /** 收费项目名称 */
    @Excel(name = "收费项目名称")
    private String cname;

    /** 最小单位 */
    @Excel(name = "最小单位")
    private BigDecimal minNum;

    /** 最大单位 */
    @Excel(name = "最大单位")
    private BigDecimal maxNum;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal money;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCid(Long cid) 
    {
        this.cid = cid;
    }

    public Long getCid() 
    {
        return cid;
    }
    public void setCname(String cname) 
    {
        this.cname = cname;
    }

    public String getCname() 
    {
        return cname;
    }
    public void setMinNum(BigDecimal minNum) 
    {
        this.minNum = minNum;
    }

    public BigDecimal getMinNum() 
    {
        return minNum;
    }
    public void setMaxNum(BigDecimal maxNum) 
    {
        this.maxNum = maxNum;
    }

    public BigDecimal getMaxNum() 
    {
        return maxNum;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cid", getCid())
            .append("cname", getCname())
            .append("minNum", getMinNum())
            .append("maxNum", getMaxNum())
            .append("money", getMoney())
            .append("unit", getUnit())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
