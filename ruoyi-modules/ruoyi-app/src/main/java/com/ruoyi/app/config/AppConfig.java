package com.ruoyi.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 代码生成相关配置
 * 
 * @author ruoyi
 */
@Component
@ConfigurationProperties(prefix = "appconfig")
public class AppConfig
{
    /** 上传路径 */
    public static String profile;

    public static String filepath;

    public static String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        AppConfig.profile = profile;
    }

    public static String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        AppConfig.filepath = filepath;
    }
}
