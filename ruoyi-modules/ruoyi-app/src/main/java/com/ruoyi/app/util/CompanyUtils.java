package com.ruoyi.app.util;

import com.google.common.collect.Maps;
import com.ruoyi.app.domain.AppCostCompany;
import com.ruoyi.common.core.annotation.Excel;
import org.jsoup.nodes.Document;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;

public class CompanyUtils {

    public static Map getCompanyInfo(String url){
        Map<String , String> result = Maps.newConcurrentMap();
        //整个html内容
        Document doc;
        try {

            Connection conn = Jsoup.connect(url).timeout(5000);
            conn.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            conn.header("Accept-Encoding", "gzip, deflate, sdch");
            conn.header("Accept-Language", "zh-CN,zh;q=0.8");
            conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
            doc = conn.get();
            Elements tablelist = doc.select(".tableYyzz td");
            if(!tablelist.isEmpty()) {
                for (Element info : tablelist) {
                    String text = info.text();
                    String value = info.select("i").text();
                    if(text.contains("统一社会信用代码")){
                        result.put("businessCode", value);
                    }else if(text.contains("法定代表人")){
                        result.put("name", value);
                    }else if(text.contains("住所")){
                        result.put("address", value);
                    }else if(text.contains("经营范围")){
                        result.put("businessScope", value);
                    }else if(text.contains("企业名称")){
                        result.put("companyName", value);
                    }
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

}
