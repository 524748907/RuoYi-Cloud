package com.ruoyi.app.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.domain.AppCostCompany;
import com.ruoyi.app.domain.AppCostProject;
import com.ruoyi.app.util.circle.Point2D;
import org.apache.ibatis.annotations.Param;
/**
 * 店铺管理Mapper接口
 * 
 * @author ziye
 * @date 2020-07-11
 */
public interface AppCostCompanyMapper 
{
    /**
     * 查询店铺管理
     *
     * @param id 店铺管理ID
     * @return 店铺管理
     */
    public AppCostCompany selectAppCostCompanyById(Long id);

    /**
     * 查询店铺管理列表
     *
     * @param appCostCompany 店铺管理
     * @return 店铺管理集合
     */
    public List<AppCostCompany> selectAppCostCompanyList(AppCostCompany appCostCompany);

    /**
     * 新增店铺管理
     *
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    public int insertAppCostCompany(AppCostCompany appCostCompany);

    /**
     * 修改店铺管理
     *
     * @param appCostCompany 店铺管理
     * @return 结果
     */
    public int updateAppCostCompany(AppCostCompany appCostCompany);

    /**
     * 删除店铺管理
     *
     * @param id 店铺管理ID
     * @return 结果
     */
    public int deleteAppCostCompanyById(Long id);

    /**
     * 批量删除店铺管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAppCostCompanyByIds(Long[] ids);

    /**
     * 查询所有经纬度
     * @return
     */
    public List<Point2D> queryPointList(AppCostCompany appCostCompany);


    public List<Map> queryCompanyList(@Param("title")String title);
    /**
     * 加载附近1公里所有经纬度
     * @return
     */
    public List<Map> queryLatLngList(@Param("lat")Double lat, @Param("lng")Double lng, @Param("distance")Integer distance);

    /**
     * 查询用户权限
     * @param userId
     * @return
     */
    public Map getUserInfo(@Param("userId")Long userId);

    /**
     * 查询完善信息用户列表
     * @return
     */
    public List<Map> queryUserList();

    public int updateUser(@Param("id")Long id, @Param("enclosure")String enclosure, @Param("color")String color);

    /**
     * 获取系统配置信息
     * @param key
     * @return
     */
    public String getConfigValue(@Param("key")String key);

    public int updatePointList(AppCostCompany appCostCompany);

    public List<Map> querySearchList(@Param("userId")Long userId, @Param("title")String title);

}
