package com.ruoyi.app.util.circle;

import java.io.Serializable;

/**
 * 坐标点
 */
public class Point2D implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    public double x;    //lng  114
    public double y;    //lat  36
    public String state;
    public Point2D(){

    }
    public Point2D(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }
    public void setX(double x) {
        this.x = x;
    }
    public double getY() {
        return y;
    }
    public void setY(double y) {
        this.y = y;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
