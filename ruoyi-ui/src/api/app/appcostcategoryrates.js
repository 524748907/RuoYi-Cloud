import request from '@/utils/request'

// 查询收费标准列表
export function listAppcostcategoryrates(query) {
  return request({
    url: '/app/appcostcategoryrates/list',
    method: 'get',
    params: query
  })
}

// 查询收费标准详细
export function getAppcostcategoryrates(id) {
  return request({
    url: '/app/appcostcategoryrates/' + id,
    method: 'get'
  })
}

// 新增收费标准
export function addAppcostcategoryrates(data) {
  return request({
    url: '/app/appcostcategoryrates',
    method: 'post',
    data: data
  })
}

// 修改收费标准
export function updateAppcostcategoryrates(data) {
  return request({
    url: '/app/appcostcategoryrates',
    method: 'put',
    data: data
  })
}

// 删除收费标准
export function delAppcostcategoryrates(id) {
  return request({
    url: '/app/appcostcategoryrates/' + id,
    method: 'delete'
  })
}
