import request from '@/utils/request'

// 查询店铺管理列表
export function listAppcostcompany(query) {
  return request({
    url: '/app/appcostcompany/list',
    method: 'get',
    params: query
  })
}

// 查询店铺管理详细
export function getAppcostcompany(id) {
  return request({
    url: '/app/appcostcompany/' + id,
    method: 'get'
  })
}

// 新增店铺管理
export function addAppcostcompany(data) {
  return request({
    url: '/app/appcostcompany',
    method: 'post',
    data: data
  })
}

// 修改店铺管理
export function updateAppcostcompany(data) {
  return request({
    url: '/app/appcostcompany',
    method: 'put',
    data: data
  })
}

// 删除店铺管理
export function delAppcostcompany(id) {
  return request({
    url: '/app/appcostcompany/' + id,
    method: 'delete'
  })
}

// 上传门头照
export function uploadCompnayPic(data) {
  return request({
    url: '/app/appcostcompany/upload',
    method: 'post',
    data: data
  })
}

// 上传营业执照
export function uploadLicense(data) {
  return request({
    url: '/app/appcostcompany/uploadLicense',
    method: 'post',
    data: data
  })
}

// 获取分类
export function queryCostProjectList() {
  return request({
    url: '/app/f/queryCostProjectList',
    method: 'get'
  })
}

// 获取所有店铺经纬度列表
export function queryPointList(query) {
  return request({
    url: '/app/appcostcompany/queryPointList',
    method: 'post',
    params: query
  })
}

export function queryUserList() {
  return request({
    url: '/app/appcostcompany/queryUserList',
    method: 'get'
  })
}


export function updateCompanyLable(data) {
  return request({
    url: '/app/appcostcompany/updateCompanyLable',
    method: 'post',
    data: data
  })
}
