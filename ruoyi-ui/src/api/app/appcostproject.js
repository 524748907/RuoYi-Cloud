import request from '@/utils/request'

// 查询收费项目列表
export function listAppcostproject(query) {
  return request({
    url: '/app/appcostproject/list',
    method: 'get',
    params: query
  })
}

// 查询收费项目详细
export function getAppcostproject(id) {
  return request({
    url: '/app/appcostproject/' + id,
    method: 'get'
  })
}

// 新增收费项目
export function addAppcostproject(data) {
  return request({
    url: '/app/appcostproject',
    method: 'post',
    data: data
  })
}

// 修改收费项目
export function updateAppcostproject(data) {
  return request({
    url: '/app/appcostproject',
    method: 'put',
    data: data
  })
}

// 删除收费项目
export function delAppcostproject(id) {
  return request({
    url: '/app/appcostproject/' + id,
    method: 'delete'
  })
}
